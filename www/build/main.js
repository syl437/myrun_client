webpackJsonp([2],{

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__about_about__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contact_contact__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_launch_navigator__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navCtrl, launchNavigator, Server, Settings, iab, socialSharing) {
        this.navCtrl = navCtrl;
        this.launchNavigator = launchNavigator;
        this.Server = Server;
        this.Settings = Settings;
        this.iab = iab;
        this.socialSharing = socialSharing;
        this.Message = "הורידו עכשיו את אפליקציית MYRUN , כל המבצעים הכי חמים במקום אחד";
        this.Image = "http://www.tapper.org.il/myrun/mainImage.jpg";
        this.AppUrl = "http://www.tapper.org.il/myrun/app";
        this.pages = [
            { title: 'אודות', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: '', type: 0 },
            { title: 'גברים', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/gbrim.html', type: 1 },
            { title: 'נשים', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/nwim.html', type: 1 },
            { title: 'שעוני דופק', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/wevni-dvpq.html', type: 1 },
            { title: 'אקססוריז', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/aqssvriz.html', type: 1 },
            { title: 'SALE', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/sale.html', type: 1 },
            { title: 'NEW', component: __WEBPACK_IMPORTED_MODULE_6__about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/new.html', type: 1 },
            { title: 'צור קשר', component: __WEBPACK_IMPORTED_MODULE_7__contact_contact__["a" /* ContactPage */], url: '', type: 0 }
        ];
        this.serverHost = Settings.ServerHost;
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.Server.getCategories('getCategories').then(function (data) {
            console.log("getCategories : ", data), _this.Categoriesrray = data;
        });
        this.Server.GetAbout('GetAbout').then(function (data) {
            console.log("About : ", data);
        });
    };
    HomePage.prototype.goToURL = function (index, url) {
        //const browser = this.iab.create(url, '_blank', 'location=no,toolbar=yes');
        var browser = this.iab.create(url);
    };
    HomePage.prototype.shareViaWhatsApp = function () {
        this.socialSharing.shareViaWhatsApp(this.Message, this.Image, this.AppUrl);
    };
    HomePage.prototype.shareViaFacebook = function () {
        var browser = this.iab.create("https://www.facebook.com/misize/?ref=profile_intro_card&pnref=lhc", '_blank', 'location=no,toolbar=yes');
        // window.location.href = "https://www.facebook.com/misize/?ref=profile_intro_card&pnref=lhc";
    };
    HomePage.prototype.shareViaPhone = function () {
        window.location.href = "tel:036438598";
    };
    HomePage.prototype.shareViaMail = function () {
        this.socialSharing.shareViaEmail('אפליקציית MYRUN', this.Message, ['sales@myrun.co.il']).then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    HomePage.prototype.shareViaWaze = function () {
        this.launchNavigator.navigate([32.123286, 34.808958]);
        //window.location.href = "waze://?ll=32.123286,34.808958"
        //<a href='waze://?ll=31.99090,34.77444'>Let's go to the mall</a>
    };
    HomePage.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        console.log(page.type);
        if (page.type == 0)
            this.navCtrl.push(page.component);
        else
            // this.browser = this.iab.create(page.url, '_blank', 'location=no,toolbar=yes');
            this.browser = this.iab.create(page.url);
        //window.location.href = page.url;
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
], HomePage.prototype, "nav", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\pages\home\home.html"*/'<menu></menu>\n\n\n\n\n\n<ion-content  class="Content">\n\n  <div class="Products">\n\n    <div class="Product" *ngFor="let item of Categoriesrray let i=index" (click)="goToURL(i,item.url)">\n\n      <img src="{{serverHost+item.image}}" style="width:100%;">\n\n      <div class="title">{{item.title}}</div>\n\n      <div class="fullDescription" align="center">\n\n        <div class="description">{{item.description}}</div>\n\n        <button ion-button class="buyButton"> לרכישה</button>\n\n      </div>\n\n    </div>\n\n    <!--<button ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">-->\n\n      <!--{{p.title}}-->\n\n    <!--</button>-->\n\n    <ion-list style="direction: rtl; background-color: #f1f1f1; margin-bottom:30px;">\n\n      <ion-item *ngFor="let p of pages" (click)="openPage(p)" style="background-color: #f1f1f1">\n\n        <h3>{{p.title}}</h3>\n\n        <button ion-button clear item-end>\n\n          <ion-icon style="color: black" name="ios-arrow-back"></ion-icon>\n\n        </button>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer class="FooterClass">\n\n  <table style="margin-right:20px; margin-top: 4px; ">\n\n    <tr>\n\n      <td (click)="shareViaWhatsApp()">\n\n        <img src="images/footer/1.png" style="width: 100%" />\n\n      </td>\n\n      <td (click)="shareViaFacebook()">\n\n        <img src="images/footer/2.png" style="width: 100%" />\n\n      </td>\n\n      <td (click)="shareViaPhone()">\n\n        <img src="images/footer/3.png" style="width: 100%" />\n\n      </td>\n\n      <td (click)="shareViaMail()">\n\n        <img src="images/footer/4.png" style="width: 100%" />\n\n      </td>\n\n      <td (click)="shareViaWaze()">\n\n          <img src="images/footer/5.png" style="width: 100%" />\n\n      </td>\n\n    </tr>\n\n  </table>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 145:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 145;

/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		576,
		1
	],
	"../pages/contact/contact.module": [
		577,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 188;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupService = (function () {
    function PopupService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    ;
    PopupService.prototype.presentAlert = function (Title, Message, PageRedirect, fun) {
        if (fun === void 0) { fun = null; }
        var alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                    text: 'סגור',
                    handler: function () { if (PageRedirect == 1) {
                        fun;
                    } }
                }],
            cssClass: 'alertRtl'
        });
        alert.present();
    };
    return PopupService;
}());
PopupService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], PopupService);

//# sourceMappingURL=popups.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(265);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_config__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_send_to_server__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_about_about__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_popups__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_menu_menu__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_social_sharing__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_launch_navigator__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_15__components_menu_menu__["a" /* MenuComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_15__components_menu_menu__["a" /* MenuComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_9__services_send_to_server__["a" /* SendToServerService */],
            __WEBPACK_IMPORTED_MODULE_8__services_config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_14__services_popups__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendToServerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SendToServerService = (function () {
    //public ServerUrl;
    function SendToServerService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.laravelHost;
    }
    ;
    SendToServerService.prototype.getCategories = function (url) {
        var _this = this;
        var body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Parray", data), _this.Categoriesrray = data; }).toPromise();
    };
    SendToServerService.prototype.GetAbout = function (url) {
        var _this = this;
        var body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.AboutArray = data; }).toPromise();
    };
    SendToServerService.prototype.sendContactDetails = function (url) {
        var body = 'name=' + this.Settings.ContactDetails['name'] + '&details=' + this.Settings.ContactDetails['details'] + '&mail=' + this.Settings.ContactDetails['mail'] + '&phone=' + this.Settings.ContactDetails['phone'];
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) { console.log("Contact : ", data); }).toPromise();
    };
    SendToServerService.prototype.RegisterPush = function (url, push_id) {
        var body = new FormData();
        body.append('push_id', push_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Parray", data); }).toPromise();
    };
    return SendToServerService;
}());
SendToServerService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
], SendToServerService);

;
//# sourceMappingURL=send_to_server.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Config = (function () {
    function Config() {
        this.laravelHost = "http://tapper.org.il/myrun/laravel/public/api/";
        this.ServerHost = "http://tapper.org.il/myrun/php/";
    }
    ;
    return Config;
}());
Config = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], Config);

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_about_about__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_push__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = (function () {
    function MyApp(platform, statusBar, iab, splashScreen, push, Server) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.iab = iab;
        this.splashScreen = splashScreen;
        this.push = push;
        this.Server = Server;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        //used for an example of ngFor and navigation
        this.pages = [
            { title: 'ראשי', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], url: '', type: 0 },
            { title: 'אודות', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: '', type: 0 },
            { title: 'גברים', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/gbrim.html', type: 1 },
            { title: 'נשים', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/nwim.html', type: 1 },
            { title: 'שעוני דופק', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/wevni-dvpq.html', type: 1 },
            { title: 'אקססוריז', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/aqssvriz.html', type: 1 },
            { title: 'SALE', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/sale.html', type: 1 },
            { title: 'NEW', component: __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */], url: 'https://www.myrun.co.il/index.php/new.html', type: 1 },
            { title: 'צור קשר', component: __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */], url: '', type: 0 }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.pushsetup();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        console.log(page.type);
        if (page.type == 0)
            this.nav.setRoot(page.component);
        else
            // this.browser = this.iab.create(page.url, '_blank', 'location=no,toolbar=yes');
            this.browser = this.iab.create(page.url);
        //window.location.href = page.url;
    };
    MyApp.prototype.pushsetup = function () {
        var _this = this;
        var options = {
            android: {
                senderID: '798786461061'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (notification.additionalData.foreground) {
            }
        });
        pushObject.on('registration').subscribe(function (registration) {
            _this.Server.RegisterPush('RegisterPush', registration.registrationId).then(function (data) {
                console.log("Push : ", data);
            });
        });
        //pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\app\app.html"*/'<ion-menu [content]="content" persistent="true" side="right">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title style="text-align: left; color: white">MY RUN MENU</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button  menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__["a" /* SendToServerService */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 574:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = ListPage_1 = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    return ListPage;
}());
ListPage = ListPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-list',template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-left></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-right>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\pages\list\list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], ListPage);

var ListPage_1;
//# sourceMappingURL=list.js.map

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = (function () {
    function MenuComponent() {
        console.log('Hello MenuComponent Component');
        this.text = 'Hello World';
    }
    return MenuComponent;
}());
MenuComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'menu',template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\components\menu\menu.html"*/'<!-- Generated template for the MenuComponent component -->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title style="margin-left: 35px; margin-top:5px;" align="center">\n\n        <img src="images/logo.png" style="width: 80%; margin-top: 7px" />\n\n    </ion-title>\n\n      <button ion-button menuToggle end color="black">\n\n          <ion-icon name="menu" style="color: black"></ion-icon>\n\n      </button>\n\n  </ion-navbar>\n\n</ion-header>'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\components\menu\menu.html"*/
    }),
    __metadata("design:paramtypes", [])
], MenuComponent);

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, Server, Settings) {
        this.navCtrl = navCtrl;
        this.Server = Server;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerHost;
        this.AboutObject = this.Server.AboutArray[0];
        this.AboutContent = this.AboutObject.content;
        this.MainImg = this.ServerUrl + '' + this.AboutObject.image;
        //this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.AboutObject.video)
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage.prototype.ngOnInit = function () {
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\pages\about\about.html"*/'\n\n    <menu></menu>\n\n\n\n    <ion-content class="patternBackground" style="margin-top:55px;">\n\n        <img src="{{MainImg}}" style="width: 100%;" />\n\n        <div class="AboutTitle">קצת על MYRUN</div>\n\n        <div class="AboutDescription" [innerHTML]="AboutContent"></div>\n\n    </ion-content>\n\n\n\n    <ion-footer class="FooterClass">\n\n        <footer></footer>\n\n    </ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\pages\about\about.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popups__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactPage = (function () {
    function ContactPage(navCtrl, Server, Settings, alertCtrl, Popup) {
        this.navCtrl = navCtrl;
        this.Server = Server;
        this.Settings = Settings;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.Contact = {
            'name': '',
            'mail': '',
            'details': '',
            'phone': '',
        };
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage.prototype.SendForm = function () {
        if (this.Contact.name.length < 3)
            this.Popup.presentAlert("שדה חובה", "יש למלא שם מלא", 0);
        else if (this.Contact.mail.length < 3)
            this.Popup.presentAlert("שדה חובה", "יש למלא מייל", 0);
        else if (this.Contact.phone.length < 3)
            this.Popup.presentAlert("שדה חובה", "יש למלא טלפון", 0);
        else if (this.Contact.details.length < 3)
            this.Popup.presentAlert("שדה חובה", "יש למלא תוכן הפנייה", 0);
        else {
            this.Settings.ContactDetails = this.Contact;
            this.Server.sendContactDetails('getContact');
            this.Popup.presentAlert("טופס נשלח בהצלחה", "תודה נחזור אליך בהקדם", 1, this.RedirectFun());
            this.Contact.name = '';
            this.Contact.mail = '';
            this.Contact.details = '';
            this.Contact.phone = '';
        }
    };
    ContactPage.prototype.RedirectFun = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Users\USER\Documents\github\myrun_client\src\pages\contact\contact.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<menu></menu>\n\n\n\n\n\n\n\n<ion-content  patternBackground>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>צור קשר</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <!--<ion-item>-->\n\n      <!--<ion-label floating>שם מלא</ion-label>-->\n\n      <!--<ion-input type="text" [(ngModel)]="Contact.name" name="name" ></ion-input>-->\n\n    <!--</ion-item>-->\n\n\n\n    <!--<ion-item>-->\n\n      <!--<ion-label floating>טלפון</ion-label>-->\n\n      <!--<ion-input type="tel" [(ngModel)]="Contact.phone" name="phone" ></ion-input>-->\n\n    <!--</ion-item>-->\n\n\n\n    <!--<ion-item>-->\n\n      <!--<ion-label floating>מייל</ion-label>-->\n\n      <!--<ion-input type="text" [(ngModel)]="Contact.mail" name="mail" ></ion-input>-->\n\n    <!--</ion-item>-->\n\n\n\n    <!--<ion-item>-->\n\n      <!--<ion-label floating>תוכן הפנייה</ion-label>-->\n\n      <!--<ion-textarea type="text" [(ngModel)]="Contact.details" name="details" ></ion-textarea>-->\n\n    <!--</ion-item>-->\n\n\n\n\n\n    <!--<div style="width: 100%; margin-top: 20px;" align="center" (click)="SendForm()">-->\n\n      <!--<div style="width: 90%">-->\n\n        <!--<button ion-button color="primary" style="width: 100%">שליחה</button>-->\n\n      <!--</div>-->\n\n    <!--</div>-->\n\n\n\n    <div class="loginMain patternBackground">\n\n      <div class="loginTitle">\n\n        צור קשר\n\n      </div>\n\n      <div class="InputsText" align="center">\n\n        <div class="InputText">\n\n          <input type="text" placeholder="הכנס שם מלא" [(ngModel)]="Contact.name" />\n\n        </div>\n\n        <div class="InputText mt10">\n\n          <input type="text" placeholder="הכנס מספר טלפון" [(ngModel)]="Contact.phone" />\n\n        </div>\n\n        <div class="InputText mt10">\n\n          <input type="text" placeholder="הכנס אימייל" [(ngModel)]="Contact.mail" />\n\n        </div>\n\n        <div class="InputText mt10">\n\n          <textarea [(ngModel)]="Contact.details" name="info" placeholder="הכניסי קצת פרטים עלייך" rows="4"></textarea>\n\n        </div>\n\n\n\n\n\n        <button class="loginButton" ion-button block (click)="SendForm()">התחל</button>\n\n      </div>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\myrun_client\src\pages\contact\contact.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__services_popups__["a" /* PopupService */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ })

},[260]);
//# sourceMappingURL=main.js.map