import {Component, OnInit, ViewChild} from '@angular/core';
import {Nav, NavController} from 'ionic-angular';
import {Config} from "../../services/config";
import {SendToServerService} from "../../services/send_to_server";
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {SocialSharing} from "@ionic-native/social-sharing";
import {AboutPage} from "../about/about";
import {ContactPage} from "../contact/contact";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {
    @ViewChild(Nav) nav: Nav;
    public Categoriesrray;
    public serverHost;
    public Message = "הורידו עכשיו את אפליקציית MYRUN , כל המבצעים הכי חמים במקום אחד";
    public Image = "http://www.tapper.org.il/myrun/mainImage.jpg"
    public AppUrl = "http://www.tapper.org.il/myrun/app"
    public browser;

    public pages = [
        {title: 'אודות', component: AboutPage , url:'',type:0},
        {title: 'גברים', component: AboutPage ,url:'https://www.myrun.co.il/index.php/gbrim.html', type:1},
        {title: 'נשים', component: AboutPage ,url:'https://www.myrun.co.il/index.php/nwim.html', type:1},
        {title: 'שעוני דופק', component: AboutPage ,url:'https://www.myrun.co.il/index.php/wevni-dvpq.html', type:1},
        {title: 'אקססוריז', component: AboutPage ,url:'https://www.myrun.co.il/index.php/aqssvriz.html', type:1},
        {title: 'SALE', component: AboutPage ,url:'https://www.myrun.co.il/index.php/sale.html', type:1},
        {title: 'NEW', component: AboutPage ,url:'https://www.myrun.co.il/index.php/new.html', type:1},
        {title: 'צור קשר', component: ContactPage ,url:'', type:0}
    ];

    constructor(public navCtrl: NavController,private launchNavigator: LaunchNavigator, public Server: SendToServerService, public Settings: Config, private iab: InAppBrowser, private socialSharing: SocialSharing) {
        this.serverHost = Settings.ServerHost;

    }

    ngOnInit() {
        this.Server.getCategories('getCategories').then((data: any) => {
            console.log("getCategories : ", data), this.Categoriesrray = data
        });
        this.Server.GetAbout('GetAbout').then((data: any) => {
            console.log("About : ", data)
        });
    }

    goToURL(index, url) {
        //const browser = this.iab.create(url, '_blank', 'location=no,toolbar=yes');
        const browser = this.iab.create(url);
    }

    shareViaWhatsApp() {
        this.socialSharing.shareViaWhatsApp(this.Message , this.Image , this.AppUrl);
    }

    shareViaFacebook() {
        const browser = this.iab.create("https://www.facebook.com/misize/?ref=profile_intro_card&pnref=lhc", '_blank', 'location=no,toolbar=yes');
       // window.location.href = "https://www.facebook.com/misize/?ref=profile_intro_card&pnref=lhc";
    }

    shareViaPhone() {
        window.location.href = "tel:036438598";
    }

    shareViaMail() {
        this.socialSharing.shareViaEmail('אפליקציית MYRUN', this.Message, ['sales@myrun.co.il']).then(() => {
            // Success!
        }).catch(() => {
            // Error!
        });
    }

    shareViaWaze() {
        this.launchNavigator.navigate([32.123286, 34.808958]);
        //window.location.href = "waze://?ll=32.123286,34.808958"
            //<a href='waze://?ll=31.99090,34.77444'>Let's go to the mall</a>
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        console.log(page.type)
        if(page.type == 0)
            this.navCtrl.push(page.component);
        else
           // this.browser = this.iab.create(page.url, '_blank', 'location=no,toolbar=yes');
        this.browser = this.iab.create(page.url);
        //window.location.href = page.url;
    }


}
