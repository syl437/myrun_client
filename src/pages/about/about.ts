import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {SafeResourceUrl, DomSanitizer} from '@angular/platform-browser';
import {Config} from "../../services/config";

/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})

export class AboutPage implements OnInit{

  public AboutObject;
  public AboutContent;
  public MainImg;
  public ServerUrl


    constructor(public navCtrl: NavController, public Server:SendToServerService,public Settings:Config) {
        this.ServerUrl = Settings.ServerHost;
        this.AboutObject = this.Server.AboutArray[0];
        this.AboutContent = this.AboutObject.content;
        this.MainImg = this.ServerUrl+''+this.AboutObject.image;
        //this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.AboutObject.video)


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');

  }

  ngOnInit()
  {
  }



}
