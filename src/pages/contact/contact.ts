import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {Config} from "../../services/config";
import {SendToServerService} from "../../services/send_to_server";
import {PopupService} from "../../services/popups";

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

    public Contact = {
        'name':'',
        'mail':'',
        'details':'',
        'phone':'',
    }



    constructor(public navCtrl: NavController, public Server:SendToServerService,public Settings:Config, public alertCtrl:AlertController, public Popup:PopupService) {



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  SendForm()
  {

      if(this.Contact.name.length < 3)
          this.Popup.presentAlert("שדה חובה" ,"יש למלא שם מלא", 0)

      else if(this.Contact.mail.length < 3)
          this.Popup.presentAlert("שדה חובה" ,"יש למלא מייל", 0)

      else if(this.Contact.phone.length < 3)
          this.Popup.presentAlert("שדה חובה" ,"יש למלא טלפון", 0)

      else if(this.Contact.details.length < 3)
          this.Popup.presentAlert("שדה חובה" ,"יש למלא תוכן הפנייה", 0)
      else
      {

          this.Settings.ContactDetails = this.Contact;
          this.Server.sendContactDetails('getContact');
          this.Popup.presentAlert( "טופס נשלח בהצלחה" ,"תודה נחזור אליך בהקדם", 1,this.RedirectFun())
          this.Contact.name = '';
          this.Contact.mail = '';
          this.Contact.details = '';
          this.Contact.phone = '';

      }

  }

    RedirectFun()
    {
        this.navCtrl.push(HomePage);
    }

}
