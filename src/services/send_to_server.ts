
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";


@Injectable()

export class SendToServerService
{
    public Categoriesrray;
    public AboutArray;
    public ServerUrl;

    //public ServerUrl;
    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.laravelHost;
    };



    getCategories(url:string)
    {
        let body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Parray" , data), this.Categoriesrray = data }).toPromise();
    }

    GetAbout(url)
    {
        let body = new FormData();
        //body.append('uid', this.Settings.UserId.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.AboutArray = data }).toPromise();
    }

    sendContactDetails(url)
    {
        let body = 'name=' + this.Settings.ContactDetails['name']  + '&details=' + this.Settings.ContactDetails['details'] + '&mail=' +  this.Settings.ContactDetails['mail']+ '&phone=' +  this.Settings.ContactDetails['phone'];
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res).do((data)=>{console.log("Contact : " , data)}).toPromise();
    }

    RegisterPush(url,push_id)
    {
        let body = new FormData();
        body.append('push_id', push_id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Parray" , data) }).toPromise();
    }
};


