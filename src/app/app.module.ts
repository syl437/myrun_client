import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Config} from "../services/config";
import {SendToServerService} from "../services/send_to_server";
import {HttpModule} from "@angular/http";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ContactPage} from "../pages/contact/contact";
import {AboutPage} from "../pages/about/about";
import {PopupService} from "../services/popups";
import {MenuComponent} from "../components/menu/menu";
import { Push }  from "@ionic-native/push";
import {SocialSharing} from "@ionic-native/social-sharing";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
      ContactPage,
      AboutPage,
      MenuComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
      ContactPage,
      AboutPage,
      MenuComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
      SendToServerService,
      Config,
      InAppBrowser,
      PopupService,
      Push,
      SocialSharing,
      LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
