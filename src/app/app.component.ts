import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {ContactPage} from "../pages/contact/contact";
import {AboutPage} from "../pages/about/about";
import {Push, PushObject, PushOptions} from '@ionic-native/push';
import {Config} from "../services/config";
import {SendToServerService} from "../services/send_to_server";
import {InAppBrowser} from "@ionic-native/in-app-browser";


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage;
    browser;

    pages: Array<{ title: string, component: any ,url:string, type:number}>;

    constructor(public platform: Platform, public statusBar: StatusBar, private iab: InAppBrowser,  public splashScreen: SplashScreen, public push: Push, public Server: SendToServerService) {
        this.initializeApp();

        //used for an example of ngFor and navigation
        this.pages = [
            {title: 'ראשי', component: HomePage ,url:'', type:0 },
            {title: 'אודות', component: AboutPage , url:'',type:0},
            {title: 'גברים', component: AboutPage ,url:'https://www.myrun.co.il/index.php/gbrim.html', type:1},
            {title: 'נשים', component: AboutPage ,url:'https://www.myrun.co.il/index.php/nwim.html', type:1},
            {title: 'שעוני דופק', component: AboutPage ,url:'https://www.myrun.co.il/index.php/wevni-dvpq.html', type:1},
            {title: 'אקססוריז', component: AboutPage ,url:'https://www.myrun.co.il/index.php/aqssvriz.html', type:1},
            {title: 'SALE', component: AboutPage ,url:'https://www.myrun.co.il/index.php/sale.html', type:1},
            {title: 'NEW', component: AboutPage ,url:'https://www.myrun.co.il/index.php/new.html', type:1},
            {title: 'צור קשר', component: ContactPage ,url:'', type:0}
        ];



    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.pushsetup();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        console.log(page.type)
        if(page.type == 0)
            this.nav.setRoot(page.component);
        else
           // this.browser = this.iab.create(page.url, '_blank', 'location=no,toolbar=yes');
        this.browser = this.iab.create(page.url);
        //window.location.href = page.url;
    }


    pushsetup() {
        const options: PushOptions = {
            android: {
                senderID: '798786461061'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {}
        };

        const pushObject: PushObject = this.push.init(options);

        pushObject.on('notification').subscribe((notification: any) => {
            if (notification.additionalData.foreground) {

            }
        });

        pushObject.on('registration').subscribe((registration: any) => {
            this.Server.RegisterPush('RegisterPush', registration.registrationId).then((data: any) => {
                console.log("Push : ", data)
            });

        });

        //pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
    }


}
